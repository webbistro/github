import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      meta: {layout: 'main'},
      component: () => import('./views/Home.vue')
    },
    {
      path: '/user/:name',
      name: 'user',
      meta: {layout: 'main'},
      component: () => import('./views/User.vue')
    },
    {
      path: '/user/:name/followers',
      name: 'followers',
      meta: {layout: 'main'},
      component: () => import('./views/Followers.vue')
    },
  ]
})
