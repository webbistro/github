import axios from 'axios'

export default {
    state: {
        user: {},
        followers: {}
    },
    mutations: {
        setUser(state, user) {
            state.user = user
        },
        setFollowers(state, followers) {
            state.followers = followers
        }
    },
    actions: {
        async fetchUser({dispatch, commit}, url) {
            try {
                let user = await axios.get(url)
                return await user.data
            } catch(e) {
                console.log(e)
                return Promise.reject(e)
            }
        },
        async getUser({dispatch, commit}, user) {
            commit('setUser', user)
        },
        async getFollowers({dispatch, commit}, followers) {
            commit('setFollowers', followers)
        }
    },
    getters: {
        user: (s) => s.user,
        followers: s => s.followers
    }
}